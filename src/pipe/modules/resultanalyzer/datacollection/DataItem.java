package pipe.modules.resultanalyzer.datacollection;

import pipe.modules.resultanalyzer.IDataItem;

public class DataItem implements IDataItem {
	
	//RunTitle
	private String mRunTitle;
	
	@Override
	public String GetRunTitle() {
		return mRunTitle;
	}

	@Override
	public void SetRunTitle(String pValue) {
		mRunTitle = pValue;
	}
	
	//TimeOfTransitionFire
	private long mTimeOfTransitionFire;
	
	@Override
	public long GetTimeOfTransitionFire() {
		return mTimeOfTransitionFire;
	}

	@Override
	public void SetTimeOfTransitionFire(long pValue) {
		mTimeOfTransitionFire = pValue;
		
	}

	//PlaceName
	private String mPlaceName;
	
	@Override
	public String GetPlaceName() {
		return mPlaceName;
	}

	@Override
	public void SetPlaceName(String pValue) {
		mPlaceName = pValue;
	}

	
	//Place Value
	private String mPlaceValue;
	
	@Override
	public String GetPlaceValue() {
		return mPlaceValue;
	}

	@Override
	public void SetPlaceValue(String pValue) {
		mPlaceValue = pValue;
	}

	
	//Place Type
	private String mPlaceType;
	
	@Override
	public String GetPlaceType() {
		return mPlaceType;
	}

	@Override
	public void SetPlaceType(String pValue) {
		mPlaceType = pValue;		
	}

	

}
