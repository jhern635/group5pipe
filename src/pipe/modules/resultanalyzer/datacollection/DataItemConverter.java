package pipe.modules.resultanalyzer.datacollection;

import java.util.List;

import pipe.client.api.model.AnimationHistory;
import pipe.client.api.model.AnimationHistoryItem;

import pipe.modules.resultanalyzer.IDataItemConverter;
import pipe.modules.resultanalyzer.IDataItemList;

public class DataItemConverter implements IDataItemConverter {
	
	@Override
	public IDataItemList Convert(AnimationHistory pAnimHist) {
		//CREATE DataItemList 
		DataItemList tDataItemList = new DataItemList();
		
		//FOR EACH AnimationHistoryItem in AnimationHistory 
		List<AnimationHistoryItem> tAnimationHistoryItemList = pAnimHist.getAnimationHistoryItemList();
	
		for(AnimationHistoryItem tAnimHistItem : tAnimationHistoryItemList)
		{				
			//FOR EACH TransitionItem in AnimationHistoryItem 
			List<AnimationHistoryItem.TransitionItem> tTransitionItemList = tAnimHistItem.getSequenceOfFiredTransition();
			
			for(AnimationHistoryItem.TransitionItem tTransitionItem : tTransitionItemList)
			{			
				//PARSE Input Symbol to Get Place and Token
				List<String> tInputs = tTransitionItem.getInputSymbols();
				for(String tInput : tInputs)
				{
					//CREATE and POPULATE DataItem			
					DataItem tItem = new DataItem();
					tItem.SetRunTitle(tAnimHistItem.getTitle());
					tItem.SetTimeOfTransitionFire(tTransitionItem.getTimeTaken());
					tItem.SetPlaceName(tInput.split(":")[0]);
					tItem.SetPlaceValue(tInput.split(":")[1]);
					tItem.SetPlaceType("In");
					
					//ADD to List
					tDataItemList.AddItems(tItem);
					
				}	
					
				//PARSE Output Symbol to Get Place and Token
				List<String> tOuts = tTransitionItem.getOutputSymbols();
				for(String tOut : tOuts)
				{
					//CREATE and POPULATE DataItem			
					DataItem tItem = new DataItem();
					tItem.SetRunTitle(tAnimHistItem.getTitle());
					tItem.SetTimeOfTransitionFire(tTransitionItem.getTimeTaken());
					tItem.SetPlaceName(tOut.split(":")[0]);
					tItem.SetPlaceValue(tOut.split(":")[1]);
					tItem.SetPlaceType("Out");
					
					//ADD to List
					tDataItemList.AddItems(tItem);
					
				}		
			}
		}
		
		//RETURN DataItemList
		return tDataItemList;
	}
	
}
