package pipe.modules.resultanalyzer.datacollection;

import java.util.ArrayList;

import pipe.modules.resultanalyzer.IDataItem;
import pipe.modules.resultanalyzer.IDataItemList;

public class DataItemList implements IDataItemList {

	private ArrayList<IDataItem> mList = new ArrayList<IDataItem>();
	
	@Override
	public void ClearItems() {
		mList.clear();
	}

	@Override
	public IDataItem[] GetList() {
		IDataItem[] tArr = new IDataItem[mList.size()];
		mList.toArray(tArr);
		
		return tArr;
	}

	@Override
	public void AddItems(IDataItem pDataItem) {
		mList.add(pDataItem);
	}

	@Override
	public void AddItems(IDataItem[] pDataItemArr) {
		for(int x=0;x<pDataItemArr.length;x++)
		{
			mList.add(pDataItemArr[x]);
		}		
	}

}
