package pipe.modules.resultanalyzer.datacollection;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import pipe.modules.resultanalyzer.IDataItem;
import pipe.modules.resultanalyzer.IDataItemList;
import pipe.modules.resultanalyzer.IDataItemSerializer;

public class DataSerializer implements IDataItemSerializer {

	@Override
	public void SerializeList(String pFilePath, IDataItemList pDataItemList) throws IOException {
		//Create Output Buffer 
		FileWriter tFileWriter = new FileWriter(pFilePath, false);
		BufferedWriter tBufferedWriter = new BufferedWriter(tFileWriter);
		try
		{
			//For each IDataItemList
			for(IDataItem tItem : pDataItemList.GetList())
			{
				//Get string representation of the Object.
				String tObjStr = GetStringRepresentation(tItem);
				
				//Write the string representation of the Object.
				tBufferedWriter.write(tObjStr);
				tBufferedWriter.newLine();
			}
			
			tBufferedWriter.flush();
		}
		finally
		{
			tBufferedWriter.close();
			tFileWriter.close();
		}
	}

	@Override
	public IDataItemList DeserializeList(String pFilePath) throws IOException {
		//Create BufferedReader and List
		FileReader tReader = new FileReader(pFilePath);
		BufferedReader tBufferReader = new BufferedReader(tReader);
		IDataItemList tDataItemList = new DataItemList();
		
		try{		
			//For each Line
			String tReadLine = tBufferReader.readLine();
			
			while(tReadLine != null)
			{
				//Convert to DataItem
				IDataItem tDataItem = GenerateIDataItemFromString(tReadLine);
				
				//Add to List
				tDataItemList.AddItems(tDataItem);
			}

		}
		finally
		{
			tBufferReader.close();
			tReader.close();
		}
		
		return tDataItemList;
	}

	public static String GetStringRepresentation(IDataItem pDataItem)
	{
		StringBuilder tStringBuilder = new StringBuilder();
		tStringBuilder.append(pDataItem.GetRunTitle()).append(",");
		tStringBuilder.append(pDataItem.GetTimeOfTransitionFire()).append(" ms").append(",");
		tStringBuilder.append(pDataItem.GetPlaceName()).append(",");
		tStringBuilder.append(pDataItem.GetPlaceValue()).append(",");
		tStringBuilder.append(pDataItem.GetPlaceType());	
		
		return tStringBuilder.toString();
	}
	
	public static IDataItem GenerateIDataItemFromString(String pObjRep)
	{
		String[] tFieldArr = pObjRep.split(",");
		
		IDataItem tDataItem = new DataItem();
		tDataItem.SetRunTitle(tFieldArr[0]);
		
		long tTime = Long.parseLong(tFieldArr[1].substring(0, tFieldArr[1].indexOf(" ")));
		tDataItem.SetTimeOfTransitionFire(tTime);
		tDataItem.SetPlaceName(tFieldArr[2]);
		tDataItem.SetPlaceValue(tFieldArr[3]);
		tDataItem.SetPlaceType(tFieldArr[4]);	
		
		return tDataItem;
	}
}
