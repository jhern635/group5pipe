package pipe.modules.resultanalyzer;

import pipe.client.api.model.AnimationHistory;

public interface IDataItemConverter {

	public IDataItemList Convert(AnimationHistory pAnimHist);
	
}
