package pipe.modules.resultanalyzer;

public interface IDataItemList {

	public void ClearItems();
	public IDataItem[] GetList();
	public void AddItems(IDataItem pDataItem);
	public void AddItems(IDataItem[] pDataItemArr);
}
