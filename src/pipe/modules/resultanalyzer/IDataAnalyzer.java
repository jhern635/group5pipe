package pipe.modules.resultanalyzer;

public interface IDataAnalyzer {
	public IAnalysisResult AnalyzeData(IDataItemList pDataItemList);
	
}
