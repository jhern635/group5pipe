package pipe.modules.resultanalyzer.factory;

import pipe.modules.resultanalyzer.IDataAnalyzer;
import pipe.modules.resultanalyzer.IDataItemConverter;
import pipe.modules.resultanalyzer.IDataItemSerializer;
import pipe.modules.resultanalyzer.PackageAnalyzerController;
import pipe.modules.resultanalyzer.analyzer.DataAnalyzer;
import pipe.modules.resultanalyzer.datacollection.DataItemConverter;
import pipe.modules.resultanalyzer.datacollection.DataSerializer;

public class PackageAnalyzerControllerFactory {

	public static PackageAnalyzerController CreateController()
	{
		IDataItemConverter mDataItemConverter = new DataItemConverter();
		IDataItemSerializer mDataItemSerializer = new DataSerializer();
		IDataAnalyzer mDataAnalyzer = new DataAnalyzer();		
		PackageAnalyzerController tPackageAnalyzerController = new PackageAnalyzerController(mDataItemConverter, mDataItemSerializer, mDataAnalyzer);
		return tPackageAnalyzerController;		
	}
}
