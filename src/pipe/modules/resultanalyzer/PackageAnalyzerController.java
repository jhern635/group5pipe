package pipe.modules.resultanalyzer;

import java.awt.Container;
import java.awt.Graphics;
import java.io.IOException;

import pipe.client.api.model.AnimationHistory;

public class PackageAnalyzerController {
	IDataItemConverter mDataItemConverter;
	IDataItemSerializer mDataItemSerializer;
	IDataAnalyzer mDataAnalyzer;
	
	IDataItemList mDataItemList;
	IAnalysisResult mAnalysisResult;
	
	public PackageAnalyzerController(IDataItemConverter pDataItemConverter, IDataItemSerializer pDataItemSerializer, IDataAnalyzer pDataAnalyzer )
	{
		mDataItemConverter = pDataItemConverter;
		mDataItemSerializer = pDataItemSerializer;
		mDataAnalyzer = pDataAnalyzer;
	}

	public void SaveAnalysisData(String pFilePath)
	{
		try {
			mDataItemSerializer.SerializeList(pFilePath, mDataItemList);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void LoadAnalysisData(String pFilePath)
	{
		try {
			mDataItemList = mDataItemSerializer.DeserializeList(pFilePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
	public void ProcessFiring(AnimationHistory pAnimationHistory)
	{
		if (pAnimationHistory.getAnimationHistoryItemList()!=null && pAnimationHistory.getAnimationHistoryItemList().size()>0)
		{
			mDataItemList = mDataItemConverter.Convert(pAnimationHistory);
		}
	}
	
	public void AnalyzeData()
	{
		mAnalysisResult = mDataAnalyzer.AnalyzeData(mDataItemList);
	}
	
	public void DrawGraphic(Container pContainer)
	{		
		mAnalysisResult.DrawAnalysis(pContainer);
	}
	
	public void DrawGraphic(Container pContainer, AnalysisResultType pAnalysisResultType)
	{		
		mAnalysisResult.DrawAnalysis(pContainer, pAnalysisResultType);
	}
	
	public void OutputResult()
	{
		String tOutput = mAnalysisResult.GetResult(AnalysisResultType.TokenPlaceTransition);
		System.out.println(tOutput);
	}
	
	public int GetDataItemListLength()
	{
		return mDataItemList==null?0:mDataItemList.GetList().length;	
	}
	
}
