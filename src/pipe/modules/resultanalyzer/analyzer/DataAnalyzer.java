package pipe.modules.resultanalyzer.analyzer;

import java.util.ArrayList;

import pipe.modules.resultanalyzer.AnalysisResultType;
import pipe.modules.resultanalyzer.IAnalysisResult;
import pipe.modules.resultanalyzer.IDataAnalyzer;
import pipe.modules.resultanalyzer.IDataItem;
import pipe.modules.resultanalyzer.IDataItemList;

public class DataAnalyzer implements IDataAnalyzer {

	
	@Override
	public IAnalysisResult AnalyzeData(IDataItemList pDataItemList) {
		
		AnalysisResult tAnalysisResult = new AnalysisResult(pDataItemList);
		
		if(pDataItemList != null && pDataItemList.GetList().length > 0 )
		{
			//Get the values of the Tokens per Transition.
			DoValuesOfTokensWork(tAnalysisResult, pDataItemList);
		}
		
		return tAnalysisResult;
	}

	private void DoValuesOfTokensWork(AnalysisResult pAnalysisResult, IDataItemList pDataItemList)
	{
		long tLastFire = 0;
		double tFire = 0;
		ArrayList<double[]> tDataResult = new ArrayList<double[]>();
				
		//For each item in the list
		for(IDataItem tItem:pDataItemList.GetList())
		{
			//I am assuming that 
			//1: The List is in order of firing.
			//2: That each firing has the same transition times.
			if(tLastFire != tItem.GetTimeOfTransitionFire())
			{
				tFire++;
				tLastFire = tItem.GetTimeOfTransitionFire();
			}
			
			double tValue = 0;
			
			try
			{
				//Sanitize the value inside the tokens a bit from <#> to #
				String tStrValue = tItem.GetPlaceValue();
				tStrValue = tStrValue.replaceAll("<", "").replaceAll(">", "");
				
				tValue = Double.parseDouble(tStrValue);
			}
			catch(NumberFormatException nfe)
			{
				tValue = 0;
			}
						
			double[] tRow = new double[]{tFire,tValue};			
			tDataResult.add(tRow);
		}
		
		pAnalysisResult.AddData(AnalysisResultType.ValuesOfTokens,  tDataResult);
	}
}
