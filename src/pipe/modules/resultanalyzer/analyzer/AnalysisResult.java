package pipe.modules.resultanalyzer.analyzer;

import java.awt.Container;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import pipe.modules.resultanalyzer.AnalysisResultType;
import pipe.modules.resultanalyzer.IAnalysisResult;
import pipe.modules.resultanalyzer.IDataItem;
import pipe.modules.resultanalyzer.IDataItemList;
import pipe.modules.resultanalyzer.datacollection.DataItemList;
import pipe.modules.resultanalyzer.datacollection.DataSerializer;

public class AnalysisResult implements IAnalysisResult {

	private IDataItemList mDataItemList = new DataItemList();
	private Hashtable<AnalysisResultType,ArrayList<double[]>> mDataTable = new Hashtable<AnalysisResultType,ArrayList<double[]>>();
	
	public AnalysisResult(IDataItemList pDataItemList)
	{
		mDataItemList = pDataItemList;
	}
	
	
	@Override
	public void DrawAnalysis(Container pContainer) {
		DrawAnalysis(pContainer, AnalysisResultType.ValuesOfTokens);
	}

	@Override
	public void DrawAnalysis(Container pContainer, AnalysisResultType pAnalysisResultType) {
		if (mDataItemList != null)
		{
			if(pAnalysisResultType == AnalysisResultType.TokenPlaceTransition)
			{ 
				String tResultText = "";
				java.awt.List tAWTList = new java.awt.List(mDataItemList.GetList().length, false);
							
				for(IDataItem tDataItem: mDataItemList.GetList())
				{
					tResultText = DataSerializer.GetStringRepresentation(tDataItem);
					tAWTList.add(tResultText); 
				}
				
				pContainer.add(tAWTList);
			}
			else if(pAnalysisResultType == AnalysisResultType.ValuesOfTokens)
			{
				DrawValuesOfTokens(pContainer);
			}
			
		}
	}
	
	private void DrawValuesOfTokens(Container pContainer)
	{
		XYDataset tXYDataset = ConvertDataTableToXYDataSet(AnalysisResultType.ValuesOfTokens);
		
		JFreeChart tChart =  ChartFactory.createScatterPlot("Token Values", "Firing", "Value", tXYDataset);
		ChartPanel tChartPanel = new ChartPanel(tChart);
		pContainer.add(tChartPanel);
		
	}
	
	//Adapted from: http://stackoverflow.com/questions/6594748/making-a-scatter-plot-using-2d-array-and-jfreechart
	private XYDataset ConvertDataTableToXYDataSet(AnalysisResultType pAnalysisResultType)
	{
		ArrayList<double[]> tArrList = mDataTable.get(pAnalysisResultType);
		XYSeriesCollection tXYSeriesCollection  = new XYSeriesCollection();
		XYSeries tXYSeries = new XYSeries("Tokens");
		
		for(int x=0;x<tArrList.size();x++)	
		{
			double[] tData = tArrList.get(x);
			tXYSeries.add(tData[0], tData[1]);			
		}
		
		tXYSeriesCollection.addSeries(tXYSeries);
		
		return tXYSeriesCollection;
	}

	@Override
	public String GetResult(AnalysisResultType pAnalysisResultType) {
		
		if(pAnalysisResultType == AnalysisResultType.TokenPlaceTransition)
		{
			if (mDataItemList != null)
			{
				StringBuffer tStringBuffer = new StringBuffer();
				
				for(IDataItem tDataItem:mDataItemList.GetList())
				{
					tStringBuffer.append(DataSerializer.GetStringRepresentation(tDataItem)).append("\n");
				}
				
				return tStringBuffer.toString();
			}
			else
			{
				//DataItemList is empty.
				return "No Results";
			}
		}
		else
		{
			return "No Result for this Analysis Type.";
		}
	}
	
	public void AddData(AnalysisResultType pAnalysisResultType, ArrayList<double[]> pData)
	{
		mDataTable.put(pAnalysisResultType, pData);
	}
	
}
