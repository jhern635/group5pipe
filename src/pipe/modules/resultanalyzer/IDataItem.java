package pipe.modules.resultanalyzer;

public interface IDataItem {

	//RunTitle
	public String GetRunTitle() ;
	public void SetRunTitle(String pValue) ;
	
	//TimeOfTransitionFire
	public long GetTimeOfTransitionFire();
	public void SetTimeOfTransitionFire(long pValue);
	
	//PlaceName
	public String GetPlaceName() ;
	public void SetPlaceName(String pValue) ;
	
	//PlaceValue
	public String GetPlaceValue() ;
	public void SetPlaceValue(String pValue) ;
	
	//PlaceType
	public String GetPlaceType() ;
	public void SetPlaceType(String pValue) ;
	
	
}
