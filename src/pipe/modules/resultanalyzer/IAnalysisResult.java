package pipe.modules.resultanalyzer;

import java.awt.Container;

public interface IAnalysisResult {
 public void DrawAnalysis(Container pContainer);
 public void DrawAnalysis(Container pContainer, AnalysisResultType pAnalysisResultType);
 
 public String GetResult(AnalysisResultType pAnalysisResultType);
}

