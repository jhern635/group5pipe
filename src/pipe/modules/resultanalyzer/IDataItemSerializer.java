package pipe.modules.resultanalyzer;

import java.io.IOException;

public interface IDataItemSerializer {
	
	public void SerializeList(String pFilePath, IDataItemList pDataItemList) throws IOException;
	
	public IDataItemList DeserializeList(String pFilePath) throws IOException;	

}
