package pipe.modules.resultanalyzer.test;

import pipe.client.api.model.AnimationHistory;
import pipe.modules.resultanalyzer.IDataItemConverter;
import pipe.modules.resultanalyzer.IDataItemList;
import pipe.modules.resultanalyzer.datacollection.DataItem;
import pipe.modules.resultanalyzer.datacollection.DataItemList;

public class DataItemConverterFake implements IDataItemConverter {

	@Override
	public IDataItemList Convert(AnimationHistory pAnimHist) {
		DataItem tDataItem = new DataItem();
		
		tDataItem.SetRunTitle("Run1");
		tDataItem.SetTimeOfTransitionFire(1);
		tDataItem.SetPlaceName("P1");
		tDataItem.SetPlaceValue("1");
		tDataItem.SetPlaceType("In");	
		
		DataItemList tDataItemList = new DataItemList();
		tDataItemList.AddItems(tDataItem);
		
		return tDataItemList;
	}

}
