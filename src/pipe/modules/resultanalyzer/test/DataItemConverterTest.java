package pipe.modules.resultanalyzer.test;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import pipe.client.api.model.AnimationHistory;
import pipe.client.api.model.AnimationHistoryItem;
import pipe.client.api.model.AnimationType;
import pipe.modules.resultanalyzer.IDataItemList;
import pipe.modules.resultanalyzer.datacollection.DataItemConverter;

public class DataItemConverterTest {

	
	//After DataItemConverter.Convert is called it should have results
	@Test
	public void DataItemConverter_Convert_HasResults() {
		//Assemble
		AnimationHistory tAnimationHistory = new AnimationHistory("Test");
		AnimationHistoryItem tAnimationHistoryItem = tAnimationHistory.addNewItem(AnimationType.SingleRandomFiring,"TestItem");
		ArrayList<String> tInput = new ArrayList<String>();
		ArrayList<String> tOutput = new ArrayList<String>();
		
		tInput.add("P1:1");
		tOutput.add("P3:2");
		AnimationHistoryItem.TransitionItem tTransitionItem = new AnimationHistoryItem.TransitionItem("RunTest", 1, tInput, tOutput);
		tAnimationHistoryItem.addFiredTransition(tTransitionItem);
		DataItemConverter tDataItemConverter = new DataItemConverter();
		
		//Act
		IDataItemList tDataItemList = tDataItemConverter.Convert(tAnimationHistory);
		
		//Assert
		Assert.assertTrue(tDataItemList.GetList().length > 0);	
	}
	
	//After DataItemConverter.Convert is called the Items should have the Title of the AnimationHistoryItem.
	@Test
	public void DataItemConverter_Convert_RunTitleIsValid() {
		//Assemble
		String tExpected = "Run1";
		AnimationHistory tAnimationHistory = new AnimationHistory("Test.xml");
		AnimationHistoryItem tAnimationHistoryItem = tAnimationHistory.addNewItem(AnimationType.SingleRandomFiring,"TestItem");
		ArrayList<String> tInput = new ArrayList<String>();
		ArrayList<String> tOutput = new ArrayList<String>();
		
		tInput.add("P1:1");
		tOutput.add("P3:2");
		AnimationHistoryItem.TransitionItem tTransitionItem = new AnimationHistoryItem.TransitionItem(tExpected, 1, tInput, tOutput);
		tAnimationHistoryItem.addFiredTransition(tTransitionItem);
		DataItemConverter tDataItemConverter = new DataItemConverter();	
	
		//Act
		IDataItemList tDataItemList = tDataItemConverter.Convert(tAnimationHistory);
		
		//Assert
		Assert.assertEquals(tExpected, tDataItemList.GetList()[0].GetRunTitle());
	}
			
	//After DataItemConverter.Convert is called the Items should have the PlaceName filled for the In.
	@Test
	public void DataItemConverter_Convert_InPlaceNameIsValid() {
		//Assemble
		String tExpected = "P1";
		AnimationHistory tAnimationHistory = new AnimationHistory(tExpected);
		AnimationHistoryItem tAnimationHistoryItem = tAnimationHistory.addNewItem(AnimationType.SingleRandomFiring,"TestItem");
		ArrayList<String> tInput = new ArrayList<String>();
		ArrayList<String> tOutput = new ArrayList<String>();
		
		tInput.add("P1:1");
		tOutput.add("P3:2");
		AnimationHistoryItem.TransitionItem tTransitionItem = new AnimationHistoryItem.TransitionItem("RunTest", 1, tInput, tOutput);
		tAnimationHistoryItem.addFiredTransition(tTransitionItem);
		DataItemConverter tDataItemConverter = new DataItemConverter();	
	
		//Act
		IDataItemList tDataItemList = tDataItemConverter.Convert(tAnimationHistory);
		
		//Assert
		Assert.assertEquals(tExpected, tDataItemList.GetList()[0].GetPlaceName());
	}
	
	//After DataItemConverter.Convert is called the Items should have the PlaceName filled for the Out.
	@Test
	public void DataItemConverter_Convert_OutPlaceNameIsValid() {
		//Assemble
		String tExpected = "P3";
		AnimationHistory tAnimationHistory = new AnimationHistory(tExpected);
		AnimationHistoryItem tAnimationHistoryItem = tAnimationHistory.addNewItem(AnimationType.SingleRandomFiring,"TestItem");
		ArrayList<String> tInput = new ArrayList<String>();
		ArrayList<String> tOutput = new ArrayList<String>();
		
		tInput.add("P1:1");
		tOutput.add("P3:2");
		AnimationHistoryItem.TransitionItem tTransitionItem = new AnimationHistoryItem.TransitionItem("RunTest", 1, tInput, tOutput);
		tAnimationHistoryItem.addFiredTransition(tTransitionItem);
		DataItemConverter tDataItemConverter = new DataItemConverter();	
	
		//Act
		IDataItemList tDataItemList = tDataItemConverter.Convert(tAnimationHistory);
		
		//Assert
		Assert.assertEquals(tExpected, tDataItemList.GetList()[1].GetPlaceName());
	}
	
	//After DataItemConverter.Convert is called the Items should have the PlaceValue filled for the In.
	@Test
	public void DataItemConverter_Convert_InPlaceValueIsValid() {
		//Assemble
		String tExpected = "1";
		AnimationHistory tAnimationHistory = new AnimationHistory(tExpected);
		AnimationHistoryItem tAnimationHistoryItem = tAnimationHistory.addNewItem(AnimationType.SingleRandomFiring,"TestItem");
		ArrayList<String> tInput = new ArrayList<String>();
		ArrayList<String> tOutput = new ArrayList<String>();
		
		tInput.add("P1:1");
		tOutput.add("P3:2");
		AnimationHistoryItem.TransitionItem tTransitionItem = new AnimationHistoryItem.TransitionItem("RunTest", 1, tInput, tOutput);
		tAnimationHistoryItem.addFiredTransition(tTransitionItem);
		DataItemConverter tDataItemConverter = new DataItemConverter();	
	
		//Act
		IDataItemList tDataItemList = tDataItemConverter.Convert(tAnimationHistory);
		
		//Assert
		Assert.assertEquals(tExpected, tDataItemList.GetList()[0].GetPlaceValue());
	}
	
	//After DataItemConverter.Convert is called the Items should have the PlaceValue filled for the Out.
	@Test
	public void DataItemConverter_Convert_OutPlaceValueIsValid() {
		//Assemble
		String tExpected = "2";
		AnimationHistory tAnimationHistory = new AnimationHistory(tExpected);
		AnimationHistoryItem tAnimationHistoryItem = tAnimationHistory.addNewItem(AnimationType.SingleRandomFiring,"TestItem");
		ArrayList<String> tInput = new ArrayList<String>();
		ArrayList<String> tOutput = new ArrayList<String>();
		
		tInput.add("P1:1");
		tOutput.add("P3:2");
		AnimationHistoryItem.TransitionItem tTransitionItem = new AnimationHistoryItem.TransitionItem("RunTest", 1, tInput, tOutput);
		tAnimationHistoryItem.addFiredTransition(tTransitionItem);
		DataItemConverter tDataItemConverter = new DataItemConverter();	
	
		//Act
		IDataItemList tDataItemList = tDataItemConverter.Convert(tAnimationHistory);
		
		//Assert
		Assert.assertEquals(tExpected, tDataItemList.GetList()[1].GetPlaceValue());
	}
		
	//After DataItemConverter.Convert is called the Items should have at least one In PlaceType.
	@Test
	public void DataItemConverter_Convert_InPlaceTypeIsValid() {
		//Assemble
		String tExpected = "In";
		AnimationHistory tAnimationHistory = new AnimationHistory(tExpected);
		AnimationHistoryItem tAnimationHistoryItem = tAnimationHistory.addNewItem(AnimationType.SingleRandomFiring,"TestItem");
		ArrayList<String> tInput = new ArrayList<String>();
		ArrayList<String> tOutput = new ArrayList<String>();
		
		tInput.add("P1:1");
		tOutput.add("P3:2");
		AnimationHistoryItem.TransitionItem tTransitionItem = new AnimationHistoryItem.TransitionItem("RunTest", 1, tInput, tOutput);
		tAnimationHistoryItem.addFiredTransition(tTransitionItem);
		DataItemConverter tDataItemConverter = new DataItemConverter();	
	
		//Act
		IDataItemList tDataItemList = tDataItemConverter.Convert(tAnimationHistory);
		
		//Assert
		Assert.assertEquals(tExpected, tDataItemList.GetList()[0].GetPlaceType());
	}
	
	//After DataItemConverter.Convert is called the Items should have at least one Out PlaceType.
	@Test
	public void DataItemConverter_Convert_OutPlaceTypeIsValid() {
		//Assemble
		String tExpected = "Out";
		AnimationHistory tAnimationHistory = new AnimationHistory(tExpected);
		AnimationHistoryItem tAnimationHistoryItem = tAnimationHistory.addNewItem(AnimationType.SingleRandomFiring,"TestItem");
		ArrayList<String> tInput = new ArrayList<String>();
		ArrayList<String> tOutput = new ArrayList<String>();
		
		tInput.add("P1:1");
		tOutput.add("P3:2");
		AnimationHistoryItem.TransitionItem tTransitionItem = new AnimationHistoryItem.TransitionItem("RunTest", 1, tInput, tOutput);
		tAnimationHistoryItem.addFiredTransition(tTransitionItem);
		DataItemConverter tDataItemConverter = new DataItemConverter();	
	
		//Act
		IDataItemList tDataItemList = tDataItemConverter.Convert(tAnimationHistory);
		
		//Assert
		Assert.assertEquals(tExpected, tDataItemList.GetList()[1].GetPlaceType());
	}
	
}
