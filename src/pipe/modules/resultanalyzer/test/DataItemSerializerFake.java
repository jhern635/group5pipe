package pipe.modules.resultanalyzer.test;

import java.io.IOException;

import pipe.modules.resultanalyzer.IDataItemList;
import pipe.modules.resultanalyzer.IDataItemSerializer;
import pipe.modules.resultanalyzer.datacollection.DataItem;
import pipe.modules.resultanalyzer.datacollection.DataItemList;

public class DataItemSerializerFake implements IDataItemSerializer {

	IDataItemList mDataItemList;
	
	@Override
	public void SerializeList(String pFilePath, IDataItemList pDataItemList) throws IOException {
		mDataItemList = pDataItemList;
	}

	@Override
	public IDataItemList DeserializeList(String pFilePath) throws IOException {
		
		if(mDataItemList!=null)
		{
			return mDataItemList;
		}
		
		if(pFilePath == "/tmp/test.data")
		{
			DataItem tDataItem = new DataItem();
			
			tDataItem.SetRunTitle("Run1");
			tDataItem.SetTimeOfTransitionFire(1);
			tDataItem.SetPlaceName("P1");
			tDataItem.SetPlaceValue("1");
			tDataItem.SetPlaceType("In");	
			
			DataItemList tDataItemList = new DataItemList();
			tDataItemList.AddItems(tDataItem);
			
			return tDataItemList;
		}
		
		return null;
	}

}
