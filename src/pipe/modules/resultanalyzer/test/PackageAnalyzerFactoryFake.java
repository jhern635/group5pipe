package pipe.modules.resultanalyzer.test;

import pipe.modules.resultanalyzer.IDataAnalyzer;
import pipe.modules.resultanalyzer.IDataItemConverter;
import pipe.modules.resultanalyzer.IDataItemSerializer;
import pipe.modules.resultanalyzer.PackageAnalyzerController;

public class PackageAnalyzerFactoryFake {

	public static PackageAnalyzerController CreateDefaultTest()
	{
		IDataItemConverter mDataItemConverter = new DataItemConverterFake();
		IDataItemSerializer mDataItemSerializer = new DataItemSerializerFake();
		IDataAnalyzer mDataAnalyzer = new DataAnalyzerFake();		
		PackageAnalyzerController tPackageAnalyzerController = new PackageAnalyzerController(mDataItemConverter, mDataItemSerializer, mDataAnalyzer);
		return tPackageAnalyzerController;		
	}
	
	public static PackageAnalyzerController CreateFailingDataItemConverterTest()
	{
		IDataItemConverter mDataItemConverter = new DataItemConverterFailFake();
		IDataItemSerializer mDataItemSerializer = new DataItemSerializerFake();
		IDataAnalyzer mDataAnalyzer = new DataAnalyzerFake();		
		PackageAnalyzerController tPackageAnalyzerController = new PackageAnalyzerController(mDataItemConverter, mDataItemSerializer, mDataAnalyzer);
		return tPackageAnalyzerController;		
	}
	

	public static PackageAnalyzerController CreateIOExceptionSerializerTest()
	{
		IDataItemConverter mDataItemConverter = new DataItemConverterFake();
		IDataItemSerializer mDataItemSerializer = new DataItemSerializerIOExceptionFake();
		IDataAnalyzer mDataAnalyzer = new DataAnalyzerFake();		
		PackageAnalyzerController tPackageAnalyzerController = new PackageAnalyzerController(mDataItemConverter, mDataItemSerializer, mDataAnalyzer);
		return tPackageAnalyzerController;		
	}
}
