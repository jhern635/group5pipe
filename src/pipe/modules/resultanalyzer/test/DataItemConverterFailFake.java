package pipe.modules.resultanalyzer.test;

import pipe.client.api.model.AnimationHistory;
import pipe.modules.resultanalyzer.IDataItemConverter;
import pipe.modules.resultanalyzer.IDataItemList;

public class DataItemConverterFailFake implements IDataItemConverter {

	@Override
	public IDataItemList Convert(AnimationHistory pAnimHist){
		return 1/0==1?null:null;
	}

}
