package pipe.modules.resultanalyzer.test;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import pipe.client.api.model.AnimationHistory;
import pipe.client.api.model.AnimationHistoryItem;
import pipe.client.api.model.AnimationType;
import pipe.dataLayer.Place;
import pipe.modules.resultanalyzer.PackageAnalyzerController;

public class PackageAnalyzerControllerTest {

	@Test
	public void PackageAnalyzerController_SaveAnalysisData_SaveEmptyDataItemist() {
		try
		{
			//Assemble
			PackageAnalyzerController tPackageAnalyzerController = PackageAnalyzerFactoryFake.CreateDefaultTest();
			
			//Act
			tPackageAnalyzerController.SaveAnalysisData("/tmp/test.data");
		}
		catch(Exception e)
		{
			//Assert
			Assert.fail(e.getMessage());
		}		
	}

	@Test
	public void PackageAnalyzerController_LoadAnalysisData_DoNotLoadOnEmpty() {
		//Assemble
		PackageAnalyzerController tPackageAnalyzerController = PackageAnalyzerFactoryFake.CreateDefaultTest();
		
		//Act
		tPackageAnalyzerController.LoadAnalysisData("/tmp/blank.data");
		
		//Assert		
		Assert.assertTrue(tPackageAnalyzerController.GetDataItemListLength()==0);
	}
	
	@Test
	public void PackageAnalyzerController_LoadAnalysisData_LoadData() {
		//Assemble
		PackageAnalyzerController tPackageAnalyzerController = PackageAnalyzerFactoryFake.CreateDefaultTest();
		
		//Act
		tPackageAnalyzerController.LoadAnalysisData("/tmp/test.data");
		
		//Assert		
		Assert.assertTrue(tPackageAnalyzerController.GetDataItemListLength()>0);
	}
	
	@Test
	public void PackageAnalyzerController_SaveAnalysisData_CatchIOException() {
		try
		{
			//Assemble
			PackageAnalyzerController tPackageAnalyzerController = PackageAnalyzerFactoryFake.CreateIOExceptionSerializerTest();
			
			//Act
			tPackageAnalyzerController.SaveAnalysisData("/tmp/test.data");
		}
		catch(Exception e)
		{
			//Assert
			Assert.fail(e.getMessage());
		}		
	}

	@Test
	public void PackageAnalyzerController_LoadAnalysisData_CatchIOException() {
		try
		{
			//Assemble
			PackageAnalyzerController tPackageAnalyzerController = PackageAnalyzerFactoryFake.CreateIOExceptionSerializerTest();
			
			//Act
			tPackageAnalyzerController.LoadAnalysisData("/tmp/test.data");
		}
		catch(Exception e)
		{
			//Assert
			Assert.fail("PackageAnalyzerController_LoadAnalysisData_CatchIOException: " + e.getMessage());
		}
	}
	
	
	@Test
	public void PackageAnalyzerController_ProcessFiring_DoNotProcessOnBlank() {
		//Assemble
		PackageAnalyzerController tPackageAnalyzerController = PackageAnalyzerFactoryFake.CreateFailingDataItemConverterTest();
		AnimationHistory tAnimationHistory = new AnimationHistory("Run1");
				
		//Act
		tPackageAnalyzerController.ProcessFiring(tAnimationHistory);
		
		//Assert
		Assert.assertTrue(tPackageAnalyzerController.GetDataItemListLength()==0);		
	}


}
