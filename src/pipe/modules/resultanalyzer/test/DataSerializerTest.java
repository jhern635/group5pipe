package pipe.modules.resultanalyzer.test;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import pipe.modules.resultanalyzer.IDataItem;
import pipe.modules.resultanalyzer.datacollection.DataItem;
import pipe.modules.resultanalyzer.datacollection.DataItemList;
import pipe.modules.resultanalyzer.datacollection.DataSerializer;

public class DataSerializerTest {

	@Test
	public void DataSerializer_GetStringRepresentation_IsValidResult() {
		//Assemble
		String tExpected = "Run1,1 ms,P1,1,In";
		IDataItem tDataItem = new DataItem();
		
		tDataItem.SetRunTitle("Run1");
		tDataItem.SetTimeOfTransitionFire(1);
		tDataItem.SetPlaceName("P1");
		tDataItem.SetPlaceValue("1");
		tDataItem.SetPlaceType("In");	
		
		//Act
		String tActual = DataSerializer.GetStringRepresentation(tDataItem);
		
		//Assert
		Assert.assertEquals(tExpected, tActual);
	}

	@Test
	public void DataSerializer_GenerateIDataItemFromString_IsValidRunTitle() {
		//Assemble		
		String tTestString = "Run1,1 ms,P1,1,In";
		String tExpected = "Run1";
		
		//Act
		IDataItem tDataItem = DataSerializer.GenerateIDataItemFromString(tTestString);
		
		//Assert
		Assert.assertEquals(tExpected, tDataItem.GetRunTitle());
	}
	
	@Test
	public void DataSerializer_GenerateIDataItemFromString_IsValidTimeOfTransitionFire() {
		//Assemble		
		String tTestString = "Run1,1 ms,P1,1,In";
		long tExpected = 1;
		
		//Act
		IDataItem tDataItem = DataSerializer.GenerateIDataItemFromString(tTestString);
		
		//Assert
		Assert.assertEquals(tExpected, tDataItem.GetTimeOfTransitionFire());
	}
	
	@Test
	public void DataSerializer_GenerateIDataItemFromString_IsValidPlaceName() {
		//Assemble		
		String tTestString = "Run1,1 ms,P1,1,In";
		String tExpected = "P1";
		
		//Act
		IDataItem tDataItem = DataSerializer.GenerateIDataItemFromString(tTestString);
		
		//Assert
		Assert.assertEquals(tExpected, tDataItem.GetPlaceName());
	}
	
	@Test
	public void DataSerializer_GenerateIDataItemFromString_IsValidPlaceValue() {
		//Assemble		
		String tTestString = "Run1,1 ms,P1,1,In";
		String tExpected = "1";
		
		//Act
		IDataItem tDataItem = DataSerializer.GenerateIDataItemFromString(tTestString);
		
		//Assert
		Assert.assertEquals(tExpected, tDataItem.GetPlaceValue());
	}
	
	@Test
	public void DataSerializer_GenerateIDataItemFromString_IsValidPlaceType() {
		//Assemble		
		String tTestString = "Run1,1 ms,P1,1,In";
		String tExpected = "In";
		
		//Act
		IDataItem tDataItem = DataSerializer.GenerateIDataItemFromString(tTestString);
		
		//Assert
		Assert.assertEquals(tExpected, tDataItem.GetPlaceType());
	}
	
}
