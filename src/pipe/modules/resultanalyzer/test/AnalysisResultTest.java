package pipe.modules.resultanalyzer.test;

import static org.junit.Assert.*;

import java.awt.Container;

import org.junit.Assert;
import org.junit.Test;

import pipe.modules.resultanalyzer.AnalysisResultType;
import pipe.modules.resultanalyzer.analyzer.AnalysisResult;

public class AnalysisResultTest {

	@Test
	public void AnalysisResult_DrawAnalysisGraphics_EmptyAnalysisDoesNotFail() {
		//Assemble
		AnalysisResult tAnalysisResult = new AnalysisResult(null); 	
		Container tContainer = new Container();
		
		//Act
		tAnalysisResult.DrawAnalysis(tContainer);
		
		//Assert
		//Did not fail
	}

	@Test
	public void AnalysisResult_GetResult_EmptyAnalysisResultNotEmpty() {
		//Assemble
		String tExpected = "No Results";
		AnalysisResult tAnalysisResult = new AnalysisResult(null); 
		
		//Act
		String tActual = tAnalysisResult.GetResult(AnalysisResultType.TokenPlaceTransition);
		
		//Assert
		Assert.assertEquals(tExpected, tActual);
	}

}
