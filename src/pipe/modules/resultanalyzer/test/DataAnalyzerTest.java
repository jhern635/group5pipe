package pipe.modules.resultanalyzer.test;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import pipe.modules.resultanalyzer.AnalysisResultType;
import pipe.modules.resultanalyzer.IAnalysisResult;
import pipe.modules.resultanalyzer.analyzer.DataAnalyzer;
import pipe.modules.resultanalyzer.datacollection.DataItem;
import pipe.modules.resultanalyzer.datacollection.DataItemList;

public class DataAnalyzerTest {

	@Test
	public void DataAnalyzer_AnalyzeData_AnalysisResultIsNotNull() {
		//Assemble
		DataAnalyzer tDataAnalyzer = new DataAnalyzer();
		DataItemConverterFake tDataItemConverter = new DataItemConverterFake();
		
		//Act
		IAnalysisResult tAnalysisResult = tDataAnalyzer.AnalyzeData(tDataItemConverter.Convert(null));
		
		//Assert
		Assert.assertNotNull(tAnalysisResult);
		
	}
	
	@Test
	public void DataAnalyzer_AnalyzeData_AnalysisResultGetResultIsNotEmpty() {
		//Assemble
		DataAnalyzer tDataAnalyzer = new DataAnalyzer();
		DataItemConverterFake tDataItemConverter = new DataItemConverterFake();
		
		//Act
		IAnalysisResult tAnalysisResult = tDataAnalyzer.AnalyzeData(tDataItemConverter.Convert(null));
		
		//Assert
		Assert.assertNotNull(tAnalysisResult.GetResult(AnalysisResultType.ValuesOfTokens));		
	}
	
	@Test
	public void DataAnalyzer_AnalyzeData_DoNotFailOnEmptyDataItemConverter() {
		//Assemble
		DataAnalyzer tDataAnalyzer = new DataAnalyzer();
				
		//Act
		IAnalysisResult tAnalysisResult = tDataAnalyzer.AnalyzeData(null);
		
		//Assert
		Assert.assertNotNull(tAnalysisResult.GetResult(AnalysisResultType.ValuesOfTokens));		
	}
	
	@Test
	public void DataAnalyzer_AnalyzeData_GenerateAnalyzeResultOnSingleRecord() {
		//Assemble
		DataAnalyzer tDataAnalyzer = new DataAnalyzer();
		DataItem tDataItem = new DataItem();
		
		tDataItem.SetRunTitle("Run1");
		tDataItem.SetTimeOfTransitionFire(1);
		tDataItem.SetPlaceName("P1");
		tDataItem.SetPlaceValue("1");
		tDataItem.SetPlaceType("In");	
		
		DataItemList tDataItemList = new DataItemList();
		tDataItemList.AddItems(tDataItem);
				
		//Act
		IAnalysisResult tAnalysisResult = tDataAnalyzer.AnalyzeData(tDataItemList);
		
		//Assert
		Assert.assertNotNull(tAnalysisResult.GetResult(AnalysisResultType.ValuesOfTokens));		
	}
}
