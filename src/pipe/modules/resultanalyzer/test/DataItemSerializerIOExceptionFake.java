package pipe.modules.resultanalyzer.test;

import java.io.IOException;

import pipe.modules.resultanalyzer.IDataItemList;
import pipe.modules.resultanalyzer.IDataItemSerializer;

public class DataItemSerializerIOExceptionFake implements IDataItemSerializer {

	@Override
	public void SerializeList(String pFilePath, IDataItemList pDataItemList) throws IOException {
		throw new IOException();
	}

	@Override
	public IDataItemList DeserializeList(String pFilePath) throws IOException {
		throw new IOException();
	}

}
