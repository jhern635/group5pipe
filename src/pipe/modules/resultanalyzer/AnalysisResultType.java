package pipe.modules.resultanalyzer;

public enum AnalysisResultType {
	ValuesOfTokens,
	AverageTransitionTimePerPlace,
	TokenPlaceTransition
}
